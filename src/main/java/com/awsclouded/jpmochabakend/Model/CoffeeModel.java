package com.awsclouded.jpmochabakend.Model;

enum CoffeeType {
    MOCHA, ESPRESSO, CAPPUCCINO, LATTE, HOT_CHOCOLATE, TEA
}

enum MilkType {
    SKIMMED, SEMI_SKIMMED, FULL, OAT, SOYA, NONE
}

public class CoffeeModel {

    CoffeeType coffeeType;
    MilkType milkType;
    int sugars;

    public CoffeeType getCoffeeType() {
        return coffeeType;
    }

    public void setCoffeeType(CoffeeType coffeeType) {
        this.coffeeType = coffeeType;
    }

    public MilkType getMilkType() {
        return milkType;
    }

    public void setMilkType(MilkType milkType) {
        this.milkType = milkType;
    }
}
