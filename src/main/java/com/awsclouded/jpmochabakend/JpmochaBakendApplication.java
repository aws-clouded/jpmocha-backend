package com.awsclouded.jpmochabakend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpmochaBakendApplication {

    public static void main(String[] args) {
        SpringApplication.run(JpmochaBakendApplication.class, args);
    }

}
