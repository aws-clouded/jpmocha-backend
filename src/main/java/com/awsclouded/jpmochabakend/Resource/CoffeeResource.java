package com.awsclouded.jpmochabakend.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
@ResponseStatus(HttpStatus.OK)
public class CoffeeResource {

}
